// console.log("Hello World");

// [SECTION] Exponent Operator
// An exponent operator is added to simplify the calculation for the exponent of a given number.
// Old
const firstNum = Math.pow(8,2);
console.log(firstNum);

// ES6 Update
const secondNum = 8 ** 2;

// [SECTION] Template Literals
// Old
let name = "John";
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:\n" + message);

// ES6 Updates 
// `` backticks - no need for next line (\n) & tabs
// ${variable} where $=placeholder

// Uses backticks (``)
// variables are placed inside a placeholder (${})
let message2 =  `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals & backticks: ${message2}`);

// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.`;
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
const fullName = ["Juan", "Tolits", "Dela Cruz"];
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);


// Hello Juan Tolits Dela Cruz! It's good to see you again.
// Old with concat
console.log("Hello " + fullName[0] + " " + fullName[1] + " " + fullName[2] + "! It's good to see you again.");

// Using Array Destructuring
// Allows us to unpack elements in arrays into distinct variables
// Allows developer to name array elemetns with variables instead of using index numbers
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you again.`);

// [SECTION] Object Destructuring
const person = {
	givenName: "Jane",
	maidenName: "Miles",
	familyName: "Doe",
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Hello Jane Miles Doe! It's good to see you again.
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring - must match property name
// This should be exact property name of the object.
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

// Objects can be destructed within a function's argument
function getFullName ({givenName, maidenName, familyName}) {
	console.log(`This is printed inside a function:`);
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
// check this later
// const getFullName2 = ({givenName, maidenName, familyName}) => {
// 	console.log(`This is printed inside a function:`);
// 	console.log(`${givenName} ${maidenName} ${familyName}`);
// }
// getFullName2();

// [SECTION] Arrow Function
/*	Syntax:
	let/const functionName =  () => {
		statements;
	}
*/
// Old
function hello() {
	console.log(`Old: Hello World!`);
}

hello();

// Converted
const hello2 = () => {
	console.log(`Converted: Hello World!`);
}

hello2();

// Arrow funciton with loop
const students = ["John", "Jane", "Judy"];

// John is a student
// Jane is a student
// Judy is a student

// forEach has not return
students.forEach(function(student){
	console.log(`${student} is a student.`);
}) 


students.forEach((student) => {
	console.log(`${student} is a student.`);
})

students.forEach((student) => console.log(`${student} is a student.`));

// [SECTION] Implicit Return Statement
const add = (x, y) => x + y;

let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
// 'User' is the default value in case a variable is not passed to the function
const greet = (name = 'User') => {
	return `Good evening, ${name}!`;
}

console.log(greet());
console.log(greet("John"));

// [SECTION] Class-Based Object Blueprint (template)

// Creating a class
class Car{
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
myCar.color = "White";

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);
